import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
// 方法2：工程内全局引入,方便调试
import PixelStreamLayer from './components/index'

createApp(App).use(PixelStreamLayer).mount('#app')
